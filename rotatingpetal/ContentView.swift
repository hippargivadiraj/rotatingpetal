//
//  ContentView.swift
//  rotatingpetal
//
//  Created by Leadconsultant on 11/27/19.
//  Copyright © 2019 Leadconsultant. All rights reserved.
//

import SwiftUI

struct Flower:Shape {
    //offset from center
    var pathOffset:Double = -20
    //pathwidth
    var pathWidth:Double = 100
    
    func path(in rect: CGRect) -> Path {
        var path = Path()
        
        for number in stride(from: 0, to: CGFloat.pi * 2, by: CGFloat.pi / 8 ){
            
            // rotation
            let rotation = CGAffineTransform(rotationAngle: number)
            //offset position cgaffine
            let position = rotation.concatenating(CGAffineTransform(translationX: rect.width / 2 , y: 0 ))
            
            let originalPetal = Path(ellipseIn: CGRect(x: CGFloat(pathOffset), y: 0, width: CGFloat(pathWidth), height: rect.width / 2))
            
            let rotatedPetal = originalPetal.applying(position)
            
            path.addPath(rotatedPetal)
            
        }
        
        return path
    }
    
    
    
}

struct ContentView: View {
    
    @State private var petalOffset = -20.0
    @State private var petalWidth = 100.0
    
    var body: some View {
        VStack{
            Flower(pathOffset: petalOffset, pathWidth: petalWidth)
                .fill(Color.red, style: FillStyle(eoFill: true, antialiased: true))
            
            Text("offset")
            Slider(value: $petalOffset, in: -40...40)
                .padding([.horizontal, .bottom])
            
            Text("Width")
            Slider(value: $petalWidth, in: 0...100)
                .padding(.horizontal)
            
            
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
